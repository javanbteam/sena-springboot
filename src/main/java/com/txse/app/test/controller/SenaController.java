package com.txse.app.test.controller;

import com.txse.app.config.MyWebMvcConfigurer;
import com.txse.app.result.Result;
import com.txse.app.test.model.Sena;
import com.txse.app.test.service.ISenaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;
import java.util.List;

/**
 * <b>概述</b>：<br>
 * TODO
 * <p>
 * <b>功能</b>：<br>
 * TODO
 *
 * @author Sena
 * @date 2018-03-30 15:03:14
 * @since 1.0
 */
@RestController
@Transactional
@RequestMapping("/senaTest")
public class SenaController {
    private final static Logger logger = (Logger) LoggerFactory.getLogger(SenaController.class);

    @Autowired
    private ISenaService senaService;

    @RequestMapping(value = "/")
    public List<Sena> getById(String id) {
        try {
            return senaService.get();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @PostMapping("/login")
    public Result loginVerify(String username, String password, HttpSession session) {
        Result result = new Result();
        if (session.getAttribute(MyWebMvcConfigurer.SESSION_KEY) != null && session.getAttribute(MyWebMvcConfigurer.SESSION_KEY).equals(username)) {
            result.setResult("已经登录");
            return result;
        }
        if (senaService.login(username, password)) {
            session.setAttribute(MyWebMvcConfigurer.SESSION_KEY, username);
            result.setResult("登录成功");
        } else {
            result.setCode("100001");
            result.setResult("登录失败");
        }
        return result;
    }

    @GetMapping("/user")
    public Object user(HttpSession session) {
        return session.getAttribute(MyWebMvcConfigurer.SESSION_KEY);
    }

    @RequestMapping("/goLogin")
    public void loginVerify(HttpServletResponse response) {
        String url = "/login.html";
        try {
            response.sendRedirect(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
