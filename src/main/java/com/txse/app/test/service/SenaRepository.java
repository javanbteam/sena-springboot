package com.txse.app.test.service;

import com.txse.app.test.model.Sena;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * <b>概述</b>：<br>
 * TODO
 * <p>
 * <b>功能</b>：<br>
 * TODO
 *
 * @author Sena
 * @date 2018-03-30 15:03:14
 * @since 1.0
 */
public interface SenaRepository extends JpaRepository<Sena, String>, JpaSpecificationExecutor<Sena> {

}
