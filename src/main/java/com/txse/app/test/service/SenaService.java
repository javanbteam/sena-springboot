package com.txse.app.test.service;

import com.txse.app.test.dao.ISenaMasterMybatis;
import com.txse.app.test.model.Sena;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
public class SenaService implements ISenaService {

    @Autowired
    @Qualifier("primaryJdbcTemplate")
    protected JdbcTemplate primaryJdbcTemplate;
    @Autowired
    private SenaRepository senaRepository;
    @Autowired
    private ISenaMasterMybatis senaMasterMybatis;

    @Override
    public List<Sena> get() {
        String sql = "SELECT * FROM sena_test";

        List<Sena> jdbc = (List<Sena>) primaryJdbcTemplate.query(sql, new RowMapper<Sena>() {
            @Override
            public Sena mapRow(ResultSet rs, int rowNum) throws SQLException {
                Sena sena = new Sena();
                sena.setId(rs.getString("ID"));
                sena.set_text(rs.getString("TEXT"));
                return sena;
            }
        });
        List<Sena> jpa = senaRepository.findAll();

        List<Sena> mybatis = senaMasterMybatis.queryAll();

        return jdbc;
    }

    @Override
    public boolean login(String username, String password) {
        String sql = "SELECT ID, TEXT FROM sena_test WHERE `NAME` = ? AND PASSWORD = ?";
        List<Sena> jdbc = (List<Sena>) primaryJdbcTemplate.query(sql, new RowMapper<Sena>() {
            @Override
            public Sena mapRow(ResultSet rs, int rowNum) throws SQLException {
                Sena sena = new Sena();
                sena.setId(rs.getString("ID"));
                sena.set_text(rs.getString("TEXT"));
                return sena;
            }
        }, new Object[]{username, toBinary(password)});
        BinstrToStr();
        return jdbc.size() > 0 ? true : false;
    }

    public String toBinary(String str) {
        char[] strChar = str.toCharArray();
        String result = "";
        for (int i = 0; i < strChar.length; i++) {
            result += Integer.toBinaryString(strChar[i]);
        }

        return result;
    }


    public void BinstrToStr() {
        String binStr = "1110011100010111001011011101010";
        String[] tempStr = binStr.split(" ");
        char[] tempChar = new char[tempStr.length];
        for (int i = 0; i < tempStr.length; i++) {
            tempChar[i] = BinstrToChar(tempStr[i]);
        }
        System.out.println(String.valueOf(tempChar));
    }


    //将二进制字符串转换成int数组
    public int[] BinstrToIntArray(String binStr) {
        char[] temp = binStr.toCharArray();
        int[] result = new int[temp.length];
        for (int i = 0; i < temp.length; i++) {
            result[i] = temp[i] - 48;
        }
        return result;
    }

    //将二进制转换成字符
    public char BinstrToChar(String binStr) {
        int[] temp = BinstrToIntArray(binStr);
        int sum = 0;
        for (int i = 0; i < temp.length; i++) {
            sum += temp[temp.length - 1 - i] << i;
        }
        return (char) sum;
    }
}
