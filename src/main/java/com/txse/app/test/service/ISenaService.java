package com.txse.app.test.service;

import com.txse.app.test.model.Sena;

import java.util.List;

public interface ISenaService {
    public List<Sena> get();

    public boolean login(String username, String password);
}
