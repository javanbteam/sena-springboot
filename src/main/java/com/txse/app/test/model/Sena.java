package com.txse.app.test.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Transient;

/**
 * SenaTest的POJO类
 *
 * @author Sena
 * @date 2018-03-30 15:03:14
 */
@Entity
@Table(name = "sena_test")
public class Sena implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7768637914227571159L;

    /**
     * ID
     */
    @Id
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "uuid")
    @Column(name = "ID", nullable = false, length = 32)
    private String id;

    /**
     * TEXT
     */
    @Column(name = "TEXT", nullable = true, length = 255)
    private String _text;

    /**
     * 虚拟主键
     */
    @Transient
    private String mxVirtualId;

    /**
     * ID的get方法
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * ID的set方法
     *
     * @param id
     */
    public void setId(String id) {
        if (id != null && id.trim().length() == 0) {
            this.id = null;
        } else {
            this.id = id;
        }
    }

    /**
     * TEXT的get方法
     *
     * @return _text
     */
    public String get_text() {
        return _text;
    }

    /**
     * TEXT的set方法
     *
     * @param _text
     */
    public void set_text(String _text) {
        this._text = _text;
    }

    public String getMxVirtualId() {
        return this.mxVirtualId;
    }

    public void setMxVirtualId(String mxVirtualId) {
        this.mxVirtualId = mxVirtualId;
    }

    /**
     * Hibernate通过该方法判断对象是否相等
     *
     * @return boolean
     */
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null || !(obj instanceof Sena))
            return false;

        if (getClass() != obj.getClass())
            return false;

        Sena other = (Sena) obj;

        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (_text == null) {
            if (other._text != null) {
                return false;
            }
        } else if (!_text.equals(other._text)) {
            return false;
        }
        return true;
    }

    /**
     * toString方法
     *
     * @return String
     */
    public String toString() {
        return "SenaTest [" + ", id=" + id + ", _text=" + _text;
    }

    /**
     * hashcode方法
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
