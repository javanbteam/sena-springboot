package com.txse.app.test.dao;

import com.txse.app.test.model.Sena;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ISenaMasterMybatis {
    Sena findByName(@Param("text") String text);

    List<Sena> queryAll();
}
